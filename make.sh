#!/bin/bash
#jupyter execute generate_figures.ipynb
for i in `seq 1 6`;
do
    inkscape --batch-process figures/figure$i.svg -o figures/figure$i.eps
    inkscape --batch-process figures/figure$i.svg -o figures/figure$i.png
done

for i in `seq 1 1`;
do
    inkscape --batch-process figures/figureS$i.svg -o figures/figureS$i.eps
    inkscape --batch-process figures/figureS$i.svg -o figures/figureS$i.png
done

