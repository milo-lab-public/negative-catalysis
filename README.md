[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/milo-lab-public%2Fnegative-catalysis/HEAD?labpath=figure3_interactive.ipynb)

# negative catalysis

A repository for storing and sharing scripts accompanying the manuscript for Protein Science titled: *Evolution of enzyme function and the importance of substrate-handle binding*.

# Usage

## Figures
To generate all figures (and convert them to EPS format), run the `make.sh` script from a linux command line.

Requirements:
- python 3.8+
- PyPI packages listed in `requirements.txt`
- Inkscape

## Jupyter notebook
The Jupyter notebook called `generate_figures.ipynb` runs the simulations and recreates figures 3, 5, and 6.

## Interactive figure (using Binder)
Open this [binder link](https://mybinder.org/v2/gl/milo-lab-public%2Fnegative-catalysis/HEAD?labpath=figure3_interactive.ipynb) in your browser.
Note, that it could take several minutes to load the notebook.

Then, run all the cells one by one clicking on the "play" button repeatedly. The interactive figure should appear with two adjustable rulers, one for the binding coefficient and another for the initial concentration of the binder.

## License
The MIT License (MIT)
Copyright © 2022 Elad Noor & Vijay Jayaraman

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
